﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace bookweb.stud
{
    public partial class borrow : System.Web.UI.Page
    {
        private List<Book> books = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            object obj = Session["books"];
            if (obj != null)
            {
                books = obj as List<Book>;
            }
            GridView1.DataSource = books;
            GridView1.DataBind();
        }

        //实现借书或者删除
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);//获取选择行号（从0开始）
            string bookid = GridView1.Rows[index].Cells[0].Text.ToString();//获取选择行第1列值，即图书编号
            if (e.CommandName == "Del")//移除书单
            {
                //根据图书编号移除书单中图书
                books.Remove(books.Single(b => b.no == bookid));
                GridView1.DataSource = books;// 设置书单数据源
                GridView1.DataBind(); //绑定书单数据源
                lbMsg.Text = ""; //提示信息置空
            }
            else//借书
            {
                string sno = Session["uid"].ToString();//获取学号
                //先检查已经借了多少书（最多5本）
                string sql = string.Format("SELECT COUNT(*) FROM borrow WHERE 学号='{0}'", sno);
                SqlDataReader dr = Dao.Read(sql);
                if (dr.Read() && ((int)dr[0]) >= 5)
                {
                    lbMsg.Text = "您的借书额度已满！请还书后再借哦~";
                    Dao.Close();// 释放数据库连接资源
                    return;
                }
                //再检查该书是否借过
                sql = string.Format("SELECT * FROM borrow WHERE 学号='{0}' AND 编号='{1}'", sno, bookid);
                dr = Dao.Read(sql);
                if (dr.Read())
                {
                    lbMsg.Text = "您已经借过这本书哦~";
                    Dao.Close();// 释放数据库连接资源
                    return;
                }
                // SQL语句:查哈如借书记录到数据库borrow表，借书期限90天
                sql = string.Format("INSERT INTO borrow VALUES('{0}','{1}','{2}','{3}')"
                    , sno, bookid, DateTime.Now.ToShortDateString(), DateTime.Now.AddDays(90).ToShortDateString());
                Dao.Update(sql); //执行SQL语句
                Dao.Close(); // 释放数据库连接资源
                GridView2.DataBind();// 重新绑定显示
                updateBookList(bookid, -1);//更新书单库存-1
                lbMsg.Text = "恭喜你！借书成功！";
            }
        }
        /// <summary>
        /// 更新书单，step为1库存加1，step为-1库存减1
        /// </summary>
        /// <param name="bookId">图书编号</param>
        /// <param name="step"></param>
        void updateBookList(string bookId, int step)
        {
            if (books != null)
            {
                Book book = books.Where(b => b.no == bookId).FirstOrDefault();
                if (book != null)
                {
                    book.stock += step;
                    GridView1.DataSource = books;
                    GridView1.DataBind();
                }
            }
        }

        protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);//获取选择行号（从0开始）
            string sno = Session["uid"].ToString();//获取学号
            string bookid = GridView2.Rows[index].Cells[1].Text.ToString();//获取选择行第1列值，即图书编号
            string sql = string.Format("DELETE FROM borrow WHERE 学号='{0}' AND 编号='{1}'", sno, bookid);
            Dao.Update(sql);//执行SQL语句
            Dao.Close();// 释放数据库连接资源
            GridView2.DataBind();// 重新绑定显示
            updateBookList(bookid, 1);//更新书单库存+1
            lbMsg.Text = "还书成功！";

        }
    }
}