﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="query.aspx.cs" Inherits="bookweb.stud.query" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 800px;
        }
        .auto-style3 {
            font-size: large;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>图书管理系统</h2>
            <hr />
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">
                        <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" BackColor="#B5C7DE" CssClass="auto-style3" DynamicHorizontalOffset="2" Font-Names="Verdana" Font-Size="Large" ForeColor="#284E98" StaticSubMenuIndent="10px">
                            <DynamicHoverStyle BackColor="#284E98" ForeColor="White" />
                            <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                            <DynamicMenuStyle BackColor="#B5C7DE" />
                            <DynamicSelectedStyle BackColor="#507CD1" />
                            <Items>
                                <asp:MenuItem Text="图书搜索" Value="图书搜索" Selected="True" NavigateUrl="~/stud/query.aspx"></asp:MenuItem>
                                <asp:MenuItem Text="图书借阅" Value="图书借阅" NavigateUrl="~/stud/borrow.aspx"></asp:MenuItem>
                                <asp:MenuItem Text="修改密码" Value="修改密码" NavigateUrl="~/stud/pwd.aspx"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/index.aspx" Text="退出系统" Value="退出系统"></asp:MenuItem>
                            </Items>
                            <StaticHoverStyle BackColor="#284E98" ForeColor="White" />
                            <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                            <StaticSelectedStyle BackColor="#507CD1" />
                        </asp:Menu>
                    </td>
                    <td>&nbsp;<%=Session["role"].ToString()%>:<%=Session["name"].ToString()%></td>
                </tr>
            </table>
        </div>
        <p>
            关键字:<asp:TextBox ID="txtKey" runat="server" placeholder="书名/作者/出版社"></asp:TextBox>
            <asp:Button ID="btnQuery" runat="server" Text="查找" />
        </p>
        <p>
            <asp:Label ID="lbMsg" runat="server" ForeColor="Red"></asp:Label>
        </p>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="编号" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="None" OnRowCommand="GridView1_RowCommand" Width="100%" BackColor="White" BorderColor="Black" EmptyDataText="请输入关键字进行查找~">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="编号" HeaderText="编号" ReadOnly="True" SortExpression="编号" />
                <asp:BoundField DataField="书名" HeaderText="书名" SortExpression="书名" />
                <asp:BoundField DataField="作者" HeaderText="作者" SortExpression="作者" />
                <asp:BoundField DataField="版本" HeaderText="版本" SortExpression="版本" />
                <asp:BoundField DataField="出版社" HeaderText="出版社" SortExpression="出版社" />
                <asp:BoundField DataField="库存" HeaderText="库存" SortExpression="库存" />
                <asp:CommandField SelectText="加入书单" ShowSelectButton="True" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConStr %>" SelectCommand="SELECT * FROM [book] WHERE (([书名] LIKE '%' + @书名 + '%') OR ([作者] LIKE '%' + @作者 + '%') OR ([出版社] LIKE '%' + @出版社 + '%'))">
            <SelectParameters>
                <asp:ControlParameter ControlID="txtKey" Name="书名" PropertyName="Text" />
                <asp:ControlParameter ControlID="txtKey" Name="作者" PropertyName="Text" />
                <asp:ControlParameter ControlID="txtKey" Name="出版社" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</body>
</html>
