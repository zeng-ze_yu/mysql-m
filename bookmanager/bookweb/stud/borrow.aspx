﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="borrow.aspx.cs" Inherits="bookweb.stud.borrow" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">

        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 800px;
        }
        .auto-style3 {
            font-size: large;
        }
        .auto-style4 {
            margin-top: 19px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>图书管理系统</h2>
            <hr />
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">
                        <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" BackColor="#B5C7DE" CssClass="auto-style3" DynamicHorizontalOffset="2" Font-Names="Verdana" Font-Size="Large" ForeColor="#284E98" StaticSubMenuIndent="10px">
                            <DynamicHoverStyle BackColor="#284E98" ForeColor="White" />
                            <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                            <DynamicMenuStyle BackColor="#B5C7DE" />
                            <DynamicSelectedStyle BackColor="#507CD1" />
                            <Items>
                                <asp:MenuItem Text="图书搜索" Value="图书搜索" NavigateUrl="~/stud/query.aspx"></asp:MenuItem>
                                <asp:MenuItem Text="图书借阅" Value="图书借阅" Selected="True" NavigateUrl="~/stud/borrow.aspx"></asp:MenuItem>
                                <asp:MenuItem Text="修改密码" Value="修改密码" NavigateUrl="~/stud/pwd.aspx"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/stud/query.aspx" Text="退出系统" Value="退出系统"></asp:MenuItem>
                            </Items>
                            <StaticHoverStyle BackColor="#284E98" ForeColor="White" />
                            <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                            <StaticSelectedStyle BackColor="#507CD1" />
                        </asp:Menu>
                    </td>
                    <td>&nbsp;<%=Session["role"].ToString()%>:<%=Session["name"].ToString()%></td>
                </tr>
            </table>
        </div>
        <p>
            我的书单:</p>
        <p>
            <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
            <asp:BoundField DataField="no" HeaderText="编号" />
            <asp:BoundField DataField="name" HeaderText="书名" />
            <asp:BoundField DataField="author" HeaderText="作者" />
            <asp:BoundField DataField="ver" HeaderText="版本" HtmlEncode="False" />
            <asp:BoundField DataField="press" HeaderText="出版社" />
            <asp:BoundField DataField="stock" HeaderText="库存" />
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Borrow" Text="借书"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Del" Text="删除"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>

                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
        </p>
        <p>
            <asp:Label ID="lbMsg" runat="server" ForeColor="Red"></asp:Label>
        </p>
        <p>
            我的借书:</p>
        <p>
            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="4" CssClass="auto-style4" DataKeyNames="学号,编号" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" Width="100%" OnRowCommand="GridView2_RowCommand" >
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="学号" HeaderText="学号" ReadOnly="True" SortExpression="学号" />
                    <asp:BoundField DataField="编号" HeaderText="编号" ReadOnly="True" SortExpression="编号" />
                    <asp:BoundField DataField="借书日期" HeaderText="借书日期" SortExpression="借书日期" DataFormatString="{0:yyyy-MM-dd}" />
                    <asp:BoundField DataField="归还日期" HeaderText="归还日期" SortExpression="归还日期" DataFormatString="{0:yyyy-MM-dd}" />
                    <asp:CommandField SelectText="还书" ShowSelectButton="True" />
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConStr %>" SelectCommand="SELECT * FROM [borrow] WHERE ([学号] = @学号)">
                <SelectParameters>
                    <asp:SessionParameter Name="学号" SessionField="uid" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
        </p>
        <div>
        </div>
    </form>
</body>
</html>
