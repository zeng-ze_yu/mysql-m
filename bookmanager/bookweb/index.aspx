﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="bookweb.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        body {
            width: 800px;
            margin-right: auto;
            margin-left: auto;
        }
    </style>
</head>
<body style="height: 511px">
    <form id="form1" runat="server">
        <div>
            <h2>图书管理系统</h2>
            <hr />
            <p>
                用户名：<asp:TextBox ID="txtUid" runat="server"></asp:TextBox>
            </p>
            <p>
                密&#12288;码：<asp:TextBox ID="txtPwd" runat="server" TextMode="Password"></asp:TextBox>
                <br />
            </p>
            <asp:RadioButtonList ID="rblRole" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Selected="True">学生</asp:ListItem>
                <asp:ListItem>教师</asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <asp:Label ID="txtMsg" runat="server" ForeColor="#FF3399"></asp:Label>
            <br />
            <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="登录" />
            <br />
        </div>
    </form>
</body>
</html>
