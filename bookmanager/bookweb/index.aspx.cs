﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace bookweb
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string uid = txtUid.Text.Trim();
            string pwd = txtPwd.Text.Trim();
            string role = rblRole.SelectedValue;
            if (uid == "" || pwd == "")
            {
                txtMsg.Text = "用户名或密码不能为空";
            }
            else
            {
                //SQL：根据账号查询登录用户姓名、密码
                string sql = string.Format("getUser '{0}','{1}'", role, uid);
                //调用Dao执行查询
                SqlDataReader dr = Dao.Read(sql);
                if (dr.Read())//如果用户存在
                {
                    //判断密码是否正确
                    if (pwd == (string)dr["密码"])//正确
                    {
                        //账号、密码、角色信息后面程序要用，故存在session里
                        Session["uid"] = uid;//把账号写入session
                        Session["pwd"] = pwd;//把密码写入session
                        Session["name"] = dr["姓名"] as string;//把中文用户名写入session
                        Session["role"] = role;//把角色写入session
                        switch (role)//根据不同角色跳转到对应页面
                        {
                            case "学生":
                                Response.Redirect("~/stud/query.aspx");//跳转到图书查询页面
                                break;
                            case "教师":
                                Response.Redirect("~/teach/book.aspx");//跳转到编辑图书页面
                                break;
                        }
                    }
                    else//密码错误
                    {
                        txtMsg.Text = "密码错误！";
                    }
                }
                //用户不存在
                else
                {
                    txtMsg.Text = "用户名不存在！";
                }
                Dao.Close();//关闭数据库连接

            }
        }
    }
}