﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="book.aspx.cs" Inherits="bookweb.teach.book" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">


        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 800px;
        }
        .auto-style3 {
            font-size: large;
        }
        .auto-style4 {
            margin-right: 6px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h2>图书管理系统&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </h2>
            <hr />
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">
                        <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" BackColor="#B5C7DE" CssClass="auto-style3" DynamicHorizontalOffset="2" Font-Names="Verdana" Font-Size="Large" ForeColor="#284E98" StaticSubMenuIndent="10px">
                            <DynamicHoverStyle BackColor="#284E98" ForeColor="White" />
                            <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                            <DynamicMenuStyle BackColor="#B5C7DE" />
                            <DynamicSelectedStyle BackColor="#507CD1" />
                            <Items>
                                <asp:MenuItem Text="图书编辑" Value="图书搜索" NavigateUrl="~/teach/book.aspx" Selected="True"></asp:MenuItem>
                                <asp:MenuItem Text="学生管理" Value="图书借阅" NavigateUrl="~/teach/manager.aspx"></asp:MenuItem>
                                <asp:MenuItem Text="修改密码" Value="修改密码" NavigateUrl="~/teach/pwd.aspx"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/stud/query.aspx" Text="退出系统" Value="退出系统"></asp:MenuItem>
                            </Items>
                            <StaticHoverStyle BackColor="#284E98" ForeColor="White" />
                            <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                            <StaticSelectedStyle BackColor="#507CD1" />
                        </asp:Menu>
                        <br />
                    </td>
                </tr>
            </table>
        </div>
            <table class="auto-style1">
                <tr>
                    <td>&nbsp;<%=Session["role"].ToString()%>:<%=Session["name"].ToString()%></td>
                </tr>
            </table>
        <br />
                        关键字:<asp:TextBox placeholder="书名/作者/出版社" ID="txtKey" runat="server" AutoPostBack="True" OnTextChanged="txtKey_TextChanged"></asp:TextBox>
                        <br />
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="编号" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" Width="100%">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="编号" HeaderText="编号" ReadOnly="True" SortExpression="编号" />
                                <asp:BoundField DataField="书名" HeaderText="书名" SortExpression="书名" />
                                <asp:BoundField DataField="作者" HeaderText="作者" SortExpression="作者" />
                                <asp:BoundField DataField="版本" HeaderText="版本" SortExpression="版本" />
                                <asp:BoundField DataField="出版社" HeaderText="出版社" SortExpression="出版社" />
                                <asp:BoundField DataField="库存" HeaderText="库存" SortExpression="库存" />
                                <asp:TemplateField ShowHeader="False">
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="更新"></asp:LinkButton>
                                        &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" CommandName="Edit" Text="编辑"></asp:LinkButton>
                                        &nbsp;<asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="False" CommandName="Delete" Text="删除" OnClientClick="return confirm('确认删除吗？')"> </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle BackColor="#2461BF" />
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EFF3FB" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                        </asp:GridView>
                        <asp:FormView ID="FormView1" runat="server" CssClass="auto-style4" DataKeyNames="编号" DataSourceID="SqlDataSource1" DefaultMode="Insert" Width="100%">
                            <EditItemTemplate>
                                编号:
                                <asp:Label ID="编号Label1" runat="server" Text='<%# Eval("编号") %>' />
                                <br />
                                书名:
                                <asp:TextBox ID="书名TextBox" runat="server" Text='<%# Bind("书名") %>' />
                                <br />
                                作者:
                                <asp:TextBox ID="作者TextBox" runat="server" Text='<%# Bind("作者") %>' />
                                <br />
                                版本:
                                <asp:TextBox ID="版本TextBox" runat="server" Text='<%# Bind("版本") %>' />
                                <br />
                                出版社:
                                <asp:TextBox ID="出版社TextBox" runat="server" Text='<%# Bind("出版社") %>' />
                                <br />
                                库存:
                                <asp:TextBox ID="库存TextBox" runat="server" Text='<%# Bind("库存") %>' />
                                <br />
                                <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="更新" />
                                &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
                            </EditItemTemplate>
                            <InsertItemTemplate>
                                &nbsp;<asp:TextBox placeholde="编号" ID="编号TextBox" runat="server" Text='<%# Bind("编号") %>' />
                                &nbsp;<asp:TextBox placeholde="书名" ID="书名TextBox0" runat="server" Text='<%# Bind("书名") %>' />
&nbsp;<asp:TextBox placeholde="作者" ID="作者TextBox0" runat="server" Text='<%# Bind("作者") %>' />
&nbsp;<asp:TextBox placeholde="版本" ID="版本TextBox0" runat="server" Text='<%# Bind("版本") %>' />
                                &nbsp;<asp:TextBox placeholde="出版社" ID="出版社TextBox0" runat="server" Text='<%# Bind("出版社") %>' />
                                <asp:TextBox placeholde="库存" ID="库存TextBox0" runat="server" Text='<%# Bind("库存") %>' />
                                &nbsp;&nbsp;
                                <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="插入" />
                                &nbsp;<br />
                                <br />
                                <br />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </InsertItemTemplate>
                            <ItemTemplate>
                                编号:
                                <asp:Label ID="编号Label" runat="server" Text='<%# Eval("编号") %>' />
                                <br />
                                书名:
                                <asp:Label ID="书名Label" runat="server" Text='<%# Bind("书名") %>' />
                                <br />
                                作者:
                                <asp:Label ID="作者Label" runat="server" Text='<%# Bind("作者") %>' />
                                <br />
                                版本:
                                <asp:Label ID="版本Label" runat="server" Text='<%# Bind("版本") %>' />
                                <br />
                                出版社:
                                <asp:Label ID="出版社Label" runat="server" Text='<%# Bind("出版社") %>' />
                                <br />
                                库存:
                                <asp:Label ID="库存Label" runat="server" Text='<%# Bind("库存") %>' />
                                <br />
                                <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="编辑" />
                                &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="删除" />
                            </ItemTemplate>
                        </asp:FormView>
                        <br />
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConStr %>" SelectCommand="SELECT * FROM [book] WHERE (([出版社] LIKE '%' + @出版社 + '%') OR([作者] LIKE '%' + @作者 + '%') OR([书名] LIKE '%' + @书名 + '%'))" DeleteCommand="DELETE FROM [book] WHERE [编号] = @编号
" InsertCommand="INSERT INTO [book] ([编号], [书名], [作者], [版本], [出版社], [库存]) VALUES (@编号, @书名, @作者, @版本, @出版社, @库存)" UpdateCommand="UPDATE [book] SET [书名] = @书名, [作者] = @作者, [版本] = @版本, [出版社] = @出版社, [库存] = @库存 WHERE [编号] = @编号">
                            <DeleteParameters>
                                <asp:Parameter Name="编号" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="编号" />
                                <asp:Parameter Name="书名" />
                                <asp:Parameter Name="作者" />
                                <asp:Parameter Name="版本" />
                                <asp:Parameter Name="出版社" />
                                <asp:Parameter Name="库存" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:ControlParameter ControlID="txtKey" DefaultValue="%" Name="出版社" PropertyName="Text" />
                                <asp:ControlParameter ControlID="txtKey" DefaultValue="%" Name="作者" PropertyName="Text" />
                                <asp:ControlParameter ControlID="txtKey" DefaultValue="%" Name="书名" PropertyName="Text" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="书名" />
                                <asp:Parameter Name="作者" />
                                <asp:Parameter Name="版本" />
                                <asp:Parameter Name="出版社" />
                                <asp:Parameter Name="库存" />
                                <asp:Parameter Name="编号" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
    </form>
    <p>
&nbsp;</p>
</body>
</html>
