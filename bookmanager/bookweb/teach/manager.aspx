﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="manager.aspx.cs" Inherits="bookweb.teach.manager" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">



        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 800px;
        }
        .auto-style3 {
            font-size: large;
        }
        .auto-style4 {
            width: 102%;
        }
        .auto-style5 {
            width: 1112px;
        }
    </style>
</head>
<body>
            <h2>图书管理系统&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </h2>
            <hr />
    <form id="form1" runat="server">
            <table class="auto-style4">
                <tr>

                    <td class="auto-style2">
                        <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" BackColor="#B5C7DE" CssClass="auto-style3" DynamicHorizontalOffset="2" Font-Names="Verdana" Font-Size="Large" ForeColor="#284E98" StaticSubMenuIndent="10px">
                            <DynamicHoverStyle BackColor="#284E98" ForeColor="White" />
                            <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                            <DynamicMenuStyle BackColor="#B5C7DE" />
                            <DynamicSelectedStyle BackColor="#507CD1" />
                            <Items>
                                <asp:MenuItem Text="图书编辑" Value="图书编辑" NavigateUrl="~/teach/book.aspx"></asp:MenuItem>
                                <asp:MenuItem Text="学生管理" Value="学生管理" NavigateUrl="~/teach/manager.aspx" Selected="True"></asp:MenuItem>
                                <asp:MenuItem Text="修改密码" Value="修改密码" NavigateUrl="~/teach/pwd.aspx"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/stud/query.aspx" Text="退出系统" Value="退出系统"></asp:MenuItem>
                            </Items>
                            <StaticHoverStyle BackColor="#284E98" ForeColor="White" />
                            <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                            <StaticSelectedStyle BackColor="#507CD1" />
                        </asp:Menu>
                    </td>
                </tr>
            </table>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style5">&nbsp;<%=Session["role"].ToString()%>:<%=Session["name"].ToString()%></td>
                </tr>
            </table>
        <div>
        </div>
    &nbsp;关键字:<asp:TextBox placeholder="学号/姓名" ID="txtKey2" runat="server" AutoPostBack="True" OnTextChanged="txtKey2_TextChanged"></asp:TextBox>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="学号" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" Width="76%">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="学号" HeaderText="学号" ReadOnly="True" SortExpression="学号" />
                    <asp:BoundField DataField="姓名" HeaderText="姓名" SortExpression="姓名" />
                    <asp:BoundField DataField="性别" HeaderText="性别" SortExpression="性别" />
                    <asp:BoundField DataField="专业" HeaderText="专业" SortExpression="专业" />
                    <asp:BoundField DataField="学院" HeaderText="学院" SortExpression="学院" />
                    <asp:BoundField DataField="密码" HeaderText="密码" SortExpression="密码" />
                    <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="更新"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消"></asp:LinkButton>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="编辑"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete" Text="删除" OnClientClick="return confirm('确认删除吗？')"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
            <asp:FormView ID="FormView1" runat="server" CellPadding="4" DataKeyNames="学号" DataSourceID="SqlDataSource1" DefaultMode="Insert" ForeColor="#333333" Width="100%">
                <EditItemTemplate>
                    学号:
                    <asp:Label ID="学号Label1" runat="server" Text='<%# Eval("学号") %>' />
                    <br />
                    姓名:
                    <asp:TextBox ID="姓名TextBox" runat="server" Text='<%# Bind("姓名") %>' />
                    <br />
                    性别:
                    <asp:TextBox ID="性别TextBox" runat="server" Text='<%# Bind("性别") %>' />
                    <br />
                    专业:
                    <asp:TextBox ID="专业TextBox" runat="server" Text='<%# Bind("专业") %>' />
                    <br />
                    学院:
                    <asp:TextBox ID="学院TextBox" runat="server" Text='<%# Bind("学院") %>' />
                    <br />
                    密码:
                    <asp:TextBox ID="密码TextBox" runat="server" Text='<%# Bind("密码") %>' />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="更新" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消" />
                </EditItemTemplate>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <InsertItemTemplate>
                    &nbsp;<asp:TextBox ID="学号TextBox" runat="server" Text='<%# Bind("学号") %>' />
&nbsp;<asp:TextBox ID="姓名TextBox" runat="server" Text='<%# Bind("姓名") %>' />
                    &nbsp;<asp:TextBox ID="性别TextBox" runat="server" Text='<%# Bind("性别") %>' />
                    &nbsp;<asp:TextBox ID="专业TextBox" runat="server" Text='<%# Bind("专业") %>' />
                    &nbsp;<asp:TextBox ID="学院TextBox" runat="server" Text='<%# Bind("学院") %>' />
                    &nbsp;<asp:TextBox ID="密码TextBox" runat="server" Text='<%# Bind("密码") %>' />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="插入" />
                </InsertItemTemplate>
                <ItemTemplate>
                    学号:
                    <asp:Label ID="学号Label" runat="server" Text='<%# Eval("学号") %>' />
                    <br />
                    姓名:
                    <asp:Label ID="姓名Label" runat="server" Text='<%# Bind("姓名") %>' />
                    <br />
                    性别:
                    <asp:Label ID="性别Label" runat="server" Text='<%# Bind("性别") %>' />
                    <br />
                    专业:
                    <asp:Label ID="专业Label" runat="server" Text='<%# Bind("专业") %>' />
                    <br />
                    学院:
                    <asp:Label ID="学院Label" runat="server" Text='<%# Bind("学院") %>' />
                    <br />
                    密码:
                    <asp:Label ID="密码Label" runat="server" Text='<%# Bind("密码") %>' />
                    <br />
                    <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="编辑" />
                    &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="删除" />
                </ItemTemplate>
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
            </asp:FormView>
            <br />
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConStr %>" DeleteCommand="DELETE FROM [student] WHERE [学号] = @学号" SelectCommand="SELECT * FROM [student] WHERE (([学号] LIKE '%' + @学号 + '%') OR([姓名] LIKE '%' + @姓名 + '%'))" UpdateCommand="UPDATE [student] SET  [姓名] = @姓名, [性别] = @性别, [专业] = @专业, [学院] = @学院,[密码] = @密码 WHERE [学号] = @学号" InsertCommand="INSERT INTO [student] ([学号], [姓名], [性别], [专业], [学院], [密码]) VALUES (@学号, @姓名, @性别, @专业, @学院, @密码)">
                <DeleteParameters>
                    <asp:Parameter Name="学号" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="学号" />
                    <asp:Parameter Name="姓名" />
                    <asp:Parameter Name="性别" />
                    <asp:Parameter Name="专业" />
                    <asp:Parameter Name="学院" />
                    <asp:Parameter Name="密码" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtKey2" DefaultValue="%" Name="学号" PropertyName="Text" />
                    <asp:ControlParameter ControlID="txtKey2" DefaultValue="%" Name="姓名" PropertyName="Text" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="姓名" />
                    <asp:Parameter Name="性别" />
                    <asp:Parameter Name="专业" />
                    <asp:Parameter Name="学院" />
                    <asp:Parameter Name="密码" />
                    <asp:Parameter Name="学号" />
                </UpdateParameters>
            </asp:SqlDataSource>
    </form>
</body>
</html>
