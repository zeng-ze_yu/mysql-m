﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pwd.aspx.cs" Inherits="bookweb.teach.pwd" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">

        .auto-style3 {
            font-size: large;
        }
        
        .auto-style1 {
            width: 100%;
        }
        </style>
</head>
<body>
            <h2>图书管理系统</h2>
            <hr />
    <form id="form1" runat="server">
        <div>
                        <asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" BackColor="#B5C7DE" CssClass="auto-style3" DynamicHorizontalOffset="2" Font-Names="Verdana" Font-Size="Large" ForeColor="#284E98" StaticSubMenuIndent="10px">
                            <DynamicHoverStyle BackColor="#284E98" ForeColor="White" />
                            <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                            <DynamicMenuStyle BackColor="#B5C7DE" />
                            <DynamicSelectedStyle BackColor="#507CD1" />
                            <Items>
                                <asp:MenuItem Text="图书编辑" Value="图书搜索" NavigateUrl="~/teach/book.aspx"></asp:MenuItem>
                                <asp:MenuItem Text="学生管理" Value="图书借阅" NavigateUrl="~/teach/manager.aspx"></asp:MenuItem>
                                <asp:MenuItem Text="修改密码" Value="修改密码" NavigateUrl="~/teach/pwd.aspx" Selected="True"></asp:MenuItem>
                                <asp:MenuItem NavigateUrl="~/index.aspx" Text="退出系统" Value="退出系统"></asp:MenuItem>
                            </Items>
                            <StaticHoverStyle BackColor="#284E98" ForeColor="White" />
                            <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                            <StaticSelectedStyle BackColor="#507CD1" />
                        </asp:Menu>
            <br />
        </div>
            <table class="auto-style1">
                <tr>
                    <td>&nbsp;<%=Session["role"].ToString()%>:<%=Session["name"].ToString()%></td>
                </tr>
            </table>
        <p>
                            &#12288;旧密码:<asp:TextBox ID="txtOld" runat="server" TextMode="Password"></asp:TextBox>
                        </p>
                        <p>
                            <br />
                            &#12288;新密码:<asp:TextBox ID="txtPwd" runat="server" TextMode="Password"></asp:TextBox>
                        </p>
                        <p>
                            <br />
                            确认密码:<asp:TextBox ID="txtPwd2" runat="server" TextMode="Password"></asp:TextBox>
                        </p>
                        <p>
                            <asp:Label ID="lbMsg" runat="server" ForeColor="Red"></asp:Label>
                        </p>
                        <p>
                            <asp:Button ID="btnEdit" runat="server" Text="修改" OnClick="btnEdit_Click" />
                        </p>
    </form>
</body>
</html>
